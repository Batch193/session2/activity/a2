package com.zuitt.batch193;

import java.util.HashMap;

public class HashMapActivity {

    public static void main(String[] args) {

        HashMap<String, Integer> lunchMenu = new HashMap<>();

        lunchMenu.put("KatsuCurry", 265);
        lunchMenu.put("Gyudon", 180);
        lunchMenu.put("Bibimbap", 95);
        lunchMenu.put("Okonomiyaki", 130);
        lunchMenu.put("Yakisoba", 105);

        System.out.println("The available food are: " + lunchMenu);

    }

}
