package com.zuitt.batch193;

import java.util.ArrayList;
import java.util.Scanner;

public class ArrayListActivity {

    public static void main(String[] args) {

        ArrayList<String> friends = new ArrayList<>();
        Scanner enterFriend = new Scanner(System.in);
        int x = 0;

        System.out.println("Please enter the name of your best friend: ");
        String bestfriend = enterFriend.nextLine().trim();
        friends.add(bestfriend);
        System.out.println("Please enter the name of your friend who's always a joker: ");
        String jokerfriend = enterFriend.nextLine().trim();
        friends.add(jokerfriend);
        System.out.println("Please enter the name of your friend who's so serious all the time: ");
        String seriousfriend = enterFriend.nextLine().trim();
        friends.add(seriousfriend);
        System.out.println("Please enter the name of your friend who's such a sucker for romance: ");
        String romanticfriend = enterFriend.nextLine().trim();
        friends.add(romanticfriend);
        System.out.println("Your friends are: " + friends);

    }

}
