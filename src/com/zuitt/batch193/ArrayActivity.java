package com.zuitt.batch193;

import java.util.Scanner;

public class ArrayActivity {

    public static void main(String[] args) {

        int[] intSet = new int[5];
                intSet[0] = 1;
                intSet[1] = 3;
                intSet[2] = 5;
                intSet[3] = 7;
                intSet[4] = 9;

        int x = 1;
        Scanner choiceScanner = new Scanner(System.in);

        System.out.println("Please enter choose from the following: A/B/C/D/E ");
        String choice = choiceScanner.nextLine().trim();
        switch (choice) {
            case "A":
                x=0;
                System.out.println("You've chosen " + choice + " and received $" + intSet[x]);
                break;
            case "B":
                x=1;
                System.out.println("You've chosen " + choice + " and received $" + intSet[x]);
                break;
            case "C":
                x=2;
                System.out.println("You've chosen " + choice + " and received $" + intSet[x]);
                break;
            case "D":
                x=3;
                System.out.println("You've chosen " + choice + " and received $" + intSet[x]);
                break;
            case "E":
                x=4;
                System.out.println("You've chosen " + choice + " and received $" + intSet[x]);
                break;
            default:
                System.out.println("Please Enter a valid choice!");
                System.out.println(choice + " is not one of the choices.");
        }

        //Is there a way to use operands in the case so both capital and lower case can be taken into consideration?

    }

}
